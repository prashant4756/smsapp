package com.example.prashant.smsapp;

import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.telephony.SmsManager;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class SendSMSActivity extends AppCompatActivity {
    EditText etPhone;
    EditText etBody;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_send_sms);
        etPhone=(EditText)findViewById(R.id.etPhone);
        etBody=(EditText)findViewById(R.id.etbody);
        Button btnSend=(Button)findViewById(R.id.btnSend);
        String phone=getIntent().getStringExtra("phone");
        if(phone!=null)
            etPhone.setText(phone);
        btnSend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String phone=etPhone.getText().toString();
                String body=etBody.getText().toString();
                sendSMS(phone,body);
            }
        });
    }
    public void sendSMS(String phone,String body){
        try {
            ActivityCompat.requestPermissions(SendSMSActivity.this, new String[]{"android.permission.SEND_SMS"}, 123);

            SmsManager smsManager = SmsManager.getDefault();
            smsManager.sendTextMessage(phone, null, body, null, null);
            Toast.makeText(getApplicationContext(), "SMS sent.", Toast.LENGTH_LONG).show();
            etPhone.setText("");
            etBody.setText("");
        }

        catch (Exception e) {
            Toast.makeText(getApplicationContext(), "SMS faild, please try again.", Toast.LENGTH_LONG).show();
            e.printStackTrace();
        }
    }
}

