package com.example.prashant.smsapp;

import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.prashant.smsapp.adapter.SMSBodyAdapter;

public class SMSContentActivity extends AppCompatActivity {

    String phone;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_smscontent);
        phone=getIntent().getStringExtra("phone");
        TextView tv_phone=(TextView)findViewById(R.id.tv_phone);
        tv_phone.setText("Message from "+phone);
        ImageButton button_compose=(ImageButton)findViewById(R.id.btn_compose);
        button_compose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(SMSContentActivity.this , phone, Toast.LENGTH_SHORT).show();
                Intent intent= new Intent(SMSContentActivity.this,SendSMSActivity.class);
                intent.putExtra("phone",phone);
                startActivity(intent);
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();

        DBHandler dbHandler=new DBHandler(this);
        Cursor cursor=dbHandler.fetchSmsFromReceiver(phone);
        SMSBodyEntry.replace_sms_thumb_entries(cursor);
        ListView listViewSmsContent=(ListView)findViewById(R.id.listViewSmsContent);
        if(SMSBodyEntry.get_sms_body_entries().size()>0){
            ArrayAdapter<SMSBody> arrayAdapter=new SMSBodyAdapter(this,SMSBodyEntry.get_sms_body_entries());
            listViewSmsContent.setAdapter(arrayAdapter);
            arrayAdapter.notifyDataSetChanged();
        }

    }
}
