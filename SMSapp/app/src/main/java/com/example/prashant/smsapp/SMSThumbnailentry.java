package com.example.prashant.smsapp;

import android.database.Cursor;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by PRASHANT on 08-08-2016.
 */
public class SMSThumbnailentry {
    public static List<SMSThumbnail> sms_thumb_entry_list=new ArrayList<>();
    public static List<SMSThumbnail> get_sms_thumb_entries(){
        return sms_thumb_entry_list;
    }
    public static void replace_sms_thumb_entries(Cursor cursor){
        sms_thumb_entry_list.clear();
        while (cursor.moveToNext() && cursor!=null){

            String phone=cursor.getString(cursor.getColumnIndex("phone")).toString();
            SMSThumbnail SMSThumbObj=new SMSThumbnail(phone);
            sms_thumb_entry_list.add(SMSThumbObj);

        }

    }
}
