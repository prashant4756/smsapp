package com.example.prashant.smsapp;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import java.util.ArrayList;

/**
 * Created by PRASHANT on 08-08-2016.
 */
public class DBHandler extends SQLiteOpenHelper{

    public static final String DATABASE_NAME="sms.db";
    public static final String TABLE_NAME="smstable";
    public static final String SMSID="id";
    public static final String SMSPHONE="phone";
    public static final String SMSBODY="body";


    public DBHandler(Context context) {
        super(context, DATABASE_NAME , null, 1);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL("create table "+TABLE_NAME+" ("+SMSID+" integer primary key, "+SMSPHONE+" text, "+SMSBODY+" text)");
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS "+TABLE_NAME);
        onCreate(db);
    }
    public boolean insertSms(int id, String phone, String body){
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put(SMSID, id);
        contentValues.put(SMSPHONE,phone);
        contentValues.put(SMSBODY,body);
        db.insert(TABLE_NAME,null,contentValues);
        return true;
    }
    public Cursor fetchSms(){
        SQLiteDatabase db=this.getReadableDatabase();
        Cursor cursor=db.rawQuery("select DISTINCT "+SMSPHONE+" from "+TABLE_NAME+" ORDER BY "+SMSID+" DESC",null);
        return cursor;

    }
    public Cursor fetchSmsFromReceiver(String phone){
        SQLiteDatabase db=this.getReadableDatabase();
        Cursor cursor=db.rawQuery("select "+SMSBODY+" from "+TABLE_NAME+" where "+SMSPHONE+" = '"+phone+"' ORDER BY "+SMSID+" DESC",null);

        return cursor;
    }
    public Cursor searchForQuery(String query){
        SQLiteDatabase db=this.getReadableDatabase();
        Cursor cursor=db.rawQuery("select "+SMSPHONE+","+SMSBODY+" from "+TABLE_NAME+" where "+SMSBODY+" LIKE '%"+query+"%' OR "+SMSPHONE+" LIKE '%"+query+"%' order by "+SMSID+" desc ",null);
        return cursor;
    }
}
