package com.example.prashant.smsapp.adapter;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.example.prashant.smsapp.R;
import com.example.prashant.smsapp.SMSBody;
import com.example.prashant.smsapp.SMSContentActivity;
import com.example.prashant.smsapp.SMSThumbnail;

import org.w3c.dom.Text;

import java.util.List;

/**
 * Created by PRASHANT on 08-08-2016.
 */
public class SMSBodyAdapter extends ArrayAdapter {
    private LayoutInflater mInflater;
    public SMSBodyAdapter(Context context, List<SMSBody> items) {
        super(context,0,items);
        mInflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);

    }
    @Override
    public long getItemId(int position) {

        return super.getItemId(position);
    }


    private class ViewHolder{
        public TextView tv_sms_body;
    }
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        final ViewHolder holder;
        if(convertView==null){
            convertView=mInflater.inflate(R.layout.sms_body_card,parent,false);
            holder = new ViewHolder();
            holder.tv_sms_body=(TextView)convertView.findViewById(R.id.tv_sms_body);
            convertView.setTag(holder);
        }
        else {
            holder=(ViewHolder)convertView.getTag();
        }
        SMSBody item=(SMSBody) getItem(position);
        holder.tv_sms_body.setText(item.smsBody);
        holder.tv_sms_body.setTag(holder);
        return convertView;
    }
}

