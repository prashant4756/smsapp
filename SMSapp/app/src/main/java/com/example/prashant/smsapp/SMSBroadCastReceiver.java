package com.example.prashant.smsapp;

import android.annotation.TargetApi;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.telephony.SmsMessage;
import android.util.Log;
import android.widget.Toast;

/**
 * Created by PRASHANT on 09-08-2016.
 */
public class SMSBroadCastReceiver extends BroadcastReceiver{

    public SMSBroadCastReceiver(){

    }
    @TargetApi(Build.VERSION_CODES.JELLY_BEAN)
    @Override
    public void onReceive(Context context, Intent intent) {
        Bundle bundle = intent.getExtras();

        SmsMessage[] smsmsgs = null;

        String phone="";
        int id=0;
        String str = "";
        if (bundle != null) {
            Object[] pdus = (Object[]) bundle.get("pdus");
            smsmsgs = new SmsMessage[pdus.length];

            for (int i=0; i < smsmsgs.length; i++) {
                smsmsgs[i] = SmsMessage.createFromPdu((byte[]) pdus[i]);
                phone += smsmsgs[i].getOriginatingAddress().toString();
                id=smsmsgs[i].getIndexOnIcc();
                str += smsmsgs[i].getMessageBody().toString();
                str += "\n";
            }

            Uri urisms = Uri.parse("content://sms/inbox");
            Cursor cursor = context.getContentResolver()
                    .query(urisms,
                            new String[]{"_id", "address", "date", "body",
                                    "type", "read"}, "type=" + 1, null,
                            "date" + " COLLATE LOCALIZED DESC LIMIT 1");
            Log.e("first size", cursor.getCount() + "");
            cursor.moveToFirst();
            Toast.makeText(context,cursor.getString(cursor.getColumnIndex("body"))+"",Toast.LENGTH_LONG).show();
            //update db here
            DBHandler dbHandler=new DBHandler(context);
            dbHandler.insertSms(Integer.parseInt(cursor.getString(0)), cursor.getString(1),
                    cursor.getString(cursor.getColumnIndexOrThrow("body")).toString());

        }
        NotificationManager manager;
        Notification myNotication;
        Intent intentToGo = new Intent(context, MainActivity.class);
        manager = (NotificationManager)context.getSystemService(Context.NOTIFICATION_SERVICE);
        PendingIntent pendingIntent = PendingIntent.getActivity(context, 1, intentToGo, 0);

        Notification.Builder builder = new Notification.Builder(context);

        builder.setAutoCancel(false);
        builder.setContentTitle("SMS");
        builder.setContentText("You have a new message");
        builder.setSmallIcon(R.drawable.envelope);
        builder.setContentIntent(pendingIntent);
        builder.setOngoing(true);
        builder.setSubText(str);   //API level 16
        builder.setNumber(100);
        builder.build();

        myNotication = builder.getNotification();
        myNotication.flags|=Notification.FLAG_AUTO_CANCEL;
        manager.notify(11, myNotication);

    }
}
