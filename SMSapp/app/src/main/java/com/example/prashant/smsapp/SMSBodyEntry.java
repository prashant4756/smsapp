package com.example.prashant.smsapp;

import android.database.Cursor;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by PRASHANT on 08-08-2016.
 */
public class SMSBodyEntry {
        public static List<SMSBody> sms_body_entry_list=new ArrayList<>();
        public static List<SMSBody> get_sms_body_entries(){
            return sms_body_entry_list;
        }
        public static void replace_sms_thumb_entries(Cursor cursor){
            sms_body_entry_list.clear();
            while (cursor.moveToNext() && cursor!=null){

                String content=cursor.getString(cursor.getColumnIndex("body")).toString();
                SMSBody smsBody=new SMSBody(content);
                sms_body_entry_list.add(smsBody);

            }

        }
}
