package com.example.prashant.smsapp;

import android.database.Cursor;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

public class SearchActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search);
        TextView tvQuery=(TextView)findViewById(R.id.tvSearch);
        String query=getIntent().getStringExtra("key");
        query=query.trim();
        tvQuery.setText("Searching for '"+query+"'..");
        ListView listViewSearch=(ListView)findViewById(R.id.listView_search);
        List<String> arraylist=new ArrayList<>();
        ArrayAdapter adapter=new ArrayAdapter(this,android.R.layout.simple_list_item_1,arraylist);
        DBHandler dbHandler=new DBHandler(this);
        Cursor cursor=dbHandler.searchForQuery(query);
        while(cursor.moveToNext() && cursor!=null){
            String phone=cursor.getString(cursor.getColumnIndex("phone")).toString();
            String body=cursor.getString(cursor.getColumnIndex("body")).toString();
            arraylist.add(phone+"\n"+body+"\n");
        }
        listViewSearch.setAdapter(adapter);
    }
}
