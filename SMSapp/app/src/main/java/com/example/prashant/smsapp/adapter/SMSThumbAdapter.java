package com.example.prashant.smsapp.adapter;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.example.prashant.smsapp.R;
import com.example.prashant.smsapp.SMSContentActivity;
import com.example.prashant.smsapp.SMSThumbnail;

import java.util.List;

/**
 * Created by PRASHANT on 08-08-2016.
 */
public class SMSThumbAdapter extends ArrayAdapter implements View.OnClickListener{
    private LayoutInflater mInflater;
    public SMSThumbAdapter(Context context, List<SMSThumbnail> items) {
        super(context,0,items);
        mInflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);

    }
    @Override
    public long getItemId(int position) {

        return super.getItemId(position);
    }

    @Override
    public void onClick(View v) {
        String tag = String.valueOf(v.getTag());
        Intent intent=new Intent(getContext(), SMSContentActivity.class);
        intent.putExtra("phone",tag);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        getContext().startActivity(intent);
        //Toast.makeText(getContext(), tag, Toast.LENGTH_SHORT).show();
    }

    private class ViewHolder{
        public TextView tv_thumb_phone;
        public LinearLayout outer_layout;
    }
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
       final ViewHolder holder;
        if(convertView==null){
            convertView=mInflater.inflate(R.layout.sms_thumb_layout,parent,false);
            holder = new ViewHolder();
            holder.tv_thumb_phone=(TextView)convertView.findViewById(R.id.tv_thumb_phone);
            holder.outer_layout=(LinearLayout)convertView.findViewById(R.id.outer_layout);
            convertView.setTag(holder);
        }
        else {
            holder=(ViewHolder)convertView.getTag();
        }
        SMSThumbnail item=(SMSThumbnail)getItem(position);
        holder.tv_thumb_phone.setText(item.phone);
        holder.tv_thumb_phone.setTag(holder);
        holder.outer_layout.setTag(item.phone);
        holder.outer_layout.setOnClickListener(this);
        return convertView;
    }
}
